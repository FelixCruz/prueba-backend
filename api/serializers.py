from rest_framework import serializers
from api.models import Users
from django.contrib.auth import password_validation, authenticate
from uuid import uuid4

class UserLoginSerializer(serializers.Serializer):
    id= serializers.ReadOnlyField()
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    email = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    token = serializers.CharField(required=True)
    age = serializers.IntegerField(required=False)
    imagen = serializers.CharField(required=False)
    description = serializers.CharField(required=False)
    def validate_username(self,data):
            users = Users.objects.filter(username = data)
            if len(users) !=0:
                raise serializers.validationError('Usuario ya exite, ingrese uno nuevo')
            else:
                return data
        

class UserSerializer(serializers.Serializer):
    id= serializers.ReadOnlyField()
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    token = serializers.CharField(required=False)
    age = serializers.IntegerField(required=False)
    imagen = serializers.CharField(required=False)
    description = serializers.CharField(required=False)
    
    def ctoken(self):
        Rand_token = uuid4()
        return Rand_token  
    def create(self,data):
        users = Users.objects.create(**data)
        users.username = data['email']
        users.token = self.ctoken()
        users.set_password(data['password'])
        users.save()
        return users


    def update(self, instance, data):
        instance.first_name = data.get('first_name')
        instance.last_name = data.get('last_name')
        instance.email = data.get('email', instance.email)
        instance.username = data.get('email', instance.email)
        instance.age = data.get('age',instance.age)
        instance.description = data.get('description',instance.description)
        instance.set_password(data.get('password',instance.password))
        instance.save()
        return instance

    def validate_username(self,data):
            users = Users.objects.filter(username = data)
            if len(users) !=0:
                raise serializers.validationError('Usuario ya exite, ingrese uno nuevo')
            else:
                return data