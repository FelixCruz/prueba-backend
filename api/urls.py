from django.urls import path
from api.views import UserListView,UserDetailView,UserLogin

urlpatterns = [
    path('login',UserLogin.as_view(),name='login'),
    path('users',UserListView.as_view(),name='usuariosList'),
    path('users/<int:pk>',UserDetailView.as_view(),name='usuariosDetail'),
]
