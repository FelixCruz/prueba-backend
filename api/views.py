from django.shortcuts import render
from api.models import Users
from api.serializers import UserSerializer,UserLoginSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import login,logout,authenticate

class UserLogin(APIView):
    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        #Se captura los encabezados para validar conten-type : application/json
        headers_response = request.headers
        content_type = headers_response['content-type']
        if content_type != "application/json":
           return Response({"error": "Request should have 'Content-Type' header with value 'application/json'"},
             status=403)
        
        try:
           user = authenticate(username=request.data.get('email'), password=request.data.get('password'))
           if user:
              serializer = UserLoginSerializer(user)
              return Response(serializer.data)
           else:
               return Response({"error": "Error in user or password"},status=401)
        except :
            pass


class UserListView(APIView):

    def get(self, request):
        #Se captura los encabezados para validar token y json
        headers_response = request.headers
        content_type = headers_response['content-type']
        if content_type != "application/json":
            return Response({"error": "Request should have 'Content-Type' header with value 'application/json'"},
              status=403)
        try:
            access_token = headers_response['access-token']
            token = Users.objects.get(token=access_token)
        except Users.DoesNotExist:
            return Response({"Error": "no tiene token valido"},status=403)
            
        if token and content_type == "application/json":
           user = Users.objects.all()
           serializer = UserSerializer(user, many=True)
           return Response(serializer.data)

    def post(self, request):
        headers_response = request.headers
        content_type = headers_response['content-type']
        if content_type != "application/json":
           return Response({"error": "Request should have 'Content-Type' header with value 'application/json'"},
              status=403)  

        # try:
        #     access_token=headers_response['access-token']
        #     token = Users.objects.get(token=access_token)
        # except Users.DoesNotExist:
        #     return Response({"Error": "no tiene token valido"},status=403)
            
        if  content_type == "application/json" :
           serializer = UserSerializer(data=request.data)
           if serializer.is_valid():
               serializer.save()
               return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserDetailView(APIView):
    
    def get_object(self, pk):

        try:
            return Users.objects.get(pk=pk)
        except Users.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        headers_response = request.headers
        content_type = headers_response['content-type']

        if content_type != "application/json":
           return Response({"error": "Request should have 'Content-Type' header with value 'application/json'"},
              status=403) 
        try:
            access_token=headers_response['access-token']
            token = Users.objects.get(token=access_token)
        except Users.DoesNotExist:
            return Response({"Error": "no tiene token valido"},status=403)

        if token :
           user = self.get_object(pk)
           serializer = UserSerializer(user)
        return Response(serializer.data)

    def put(self, request, pk):
        headers_response = request.headers
        content_type = headers_response['content-type']

        if content_type != "application/json":
            return Response({"error": "Request should have 'Content-Type' header with value 'application/json'"},
              status=403)
        try:
            access_token=headers_response['access-token']
            token = Users.objects.get(token=access_token)
        except Users.DoesNotExist:
            return Response({"Error": "no tiene token valido"},status=403)
            
        if token :
           user = self.get_object(pk)
           serializer = UserSerializer(user, data=request.data)
           if serializer.is_valid():
               serializer.save()
               return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk):
        headers_response = request.headers
        content_type = headers_response['content-type']

        if content_type != "application/json":
           return Response({"error": "Request should have 'Content-Type' header with value 'application/json'"},
              status=403)
        try:
            access_token=headers_response['access-token']
            token = Users.objects.get(token=access_token)
        except Users.DoesNotExist:
            return Response({"Error": "no tiene token valido"},status=403)
            
        if token :
           user = self.get_object(pk)
           serializer = UserSerializer(user, data=request.data)
           if serializer.is_valid():
               serializer.save()
               return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        headers_response = request.headers
        access_token=headers_response['access-token']
        try:
            token = Users.objects.get(token=access_token)
        except Users.DoesNotExist:
            return Response({"Error": "no tiene token valido"},status=403)
            
        if token :
           user = self.get_object(pk)
           user.delete()
           return Response(status=status.HTTP_204_NO_CONTENT)