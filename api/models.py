from django.db import models
from django.contrib.auth.models import AbstractUser

class Users(AbstractUser):
    
    first_name = models.TextField(max_length=128,null=False, blank=False)
    last_name = models.TextField(max_length=128,null=False, blank=False)
    email = models.TextField(unique=True, max_length=64)
    password = models.TextField(max_length=64,null=False, blank=False)
    token = models.CharField(max_length=64, blank=False, null=False)
    age = models.IntegerField(null=True, blank=True)
    imagen = models.TextField(max_length=128, null=True, blank=True)
    description = models.TextField(max_length=255,null=True, blank=True)

    class Meta:
        db_table = 'users'
    