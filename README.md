# Instalación y configuración del proyecto

## Instalar entorno de trabajo en el projecto
-virtualenv backenenv

## Activar entorno 'backenenv'
-source backenenv/Scripts/activate

## Instalar dependencias
-pip install -r requirements.txt

## Ejecutar Migraciones
-python manage.py migrate

## Ejecutar servidor
-python manage.py runserver
## Nota:
El token lo envío por el encabezado (headers) ejm: access-toke:"kdfjhjf24ljflkj3fflj"